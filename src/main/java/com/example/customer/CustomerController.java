package com.example.customer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CustomerController {
    int i=4;

ArrayList<Customerclass> customers = new ArrayList<>();

    public CustomerController() {
   customers.add(new Customerclass(1,"dar","M",23,"Phnom Penh"));
   customers.add(new Customerclass(2,"daro","F",21,"Kompot"));
   customers.add(new Customerclass(3,"thearo","M",24,"Riem Reap"));
    }


                        //Read all Customer
    @GetMapping("/customers")
    public ArrayList<Customerclass> getAllCustomerclass(){
        return customers;
    }

                        //Insert Customer
    @PostMapping("/customers")
    public List<Customerclass> insertCustomerclass(@RequestBody Customerauto custom){
       customers.add(new Customerclass(i,custom.getName(), custom.getGender(), custom.getAge(), custom.getAddress()));
       i++;


    return customers;

    }
                                //Read Customer by id

    @GetMapping("/customers/{id}")
    public ResponseEntity<?> getCustomercById(@PathVariable("id") Integer customerId){
        boolean found = false;
        for (Customerclass cust: customers) {
            if(cust.getId().equals(customerId)) {
                found = true;
                return ResponseEntity.ok(new Response<Customerclass>(
                        LocalDateTime.now(),
                        200,
                        "Success",
                        cust
                ));
            }
        }
        if (!found){
            return ResponseEntity.ok(new Response<Customerclass>(
                    LocalDateTime.now(),
                    404,
                    "Not Found",
                    null
            ));
        }
        return null;
    }



                        //Read Customer by name
    @GetMapping("/customers/search")
    public Customerclass finCustomerByName(@RequestParam String name){
        for (Customerclass cust : customers) {
            if(cust.getName().equals(name)){
                return cust;
            }
        }
        return null;
    }

                        //Update

@PutMapping("/customers/update/{id}")
    public Customerclass getCustomerById(@PathVariable ("id") Integer id, @RequestBody Customerauto custom){
        for(Customerclass cust: customers){
            if(cust.getId().equals(id)){
                cust.setName(custom.getName());
                cust.setGender(custom.getGender());
                cust.setAge(custom.getAge());
                cust.setAddress(custom.getAddress());
                return cust;
            }

        }
        return null;
    }
                            //delete by id

    @DeleteMapping("/customers/{id}")
            public Customerclass deleteCustomerByID(@PathVariable("id") Integer customerId){
            for (Customerclass cust: customers) {
                if(customerId == cust.getId()){
                    customers.remove(customerId-1);
                    return cust;
        }
    }
    return null;
}


}
